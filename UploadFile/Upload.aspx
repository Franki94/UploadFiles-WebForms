﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="UploadFile.Upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server" method="post" enctype="multipart/form-data">
        <div>
			<asp:FileUpload ID="fileUpload" runat="server" />
			<asp:Button ID="btnUpload" runat="server" Text="Subir"  OnClick="btnUpload_Click"/>
        </div>
    </form>
</body>
</html>
